---
layout: home

hero:
  name: 山奈Galanga
  text: 开源实用的JS函数库
  tagline: 简单引入，美味享受
  image:
    src: /logo.png
    alt: Galanga
  actions:
    - theme: brand
      text: 开箱即用
      link: /guide/what-is-vitepress
    - theme: alt
      text: 查看GitHub
      link: https://github.com/censujiang/galanga?from=galanga_docs

features:
  - icon: 🛠️
    title: Simple and minimal, always
    details: Lorem ipsum...
  - icon:
      src: /cool-feature-icon.svg
    title: Another cool feature
    details: Lorem ipsum...
  - icon:
      dark: /dark-feature-icon.svg
      light: /light-feature-icon.svg
    title: Another cool feature
    details: Lorem ipsum...
---
